# CLTV-Chef

These are the cookbooks for [City Link TV](http://citylinktv.com/) used in [AWS OpsWorks](http://aws.amazon.com/opsworks/).

These cookbooks were taken from various sources across the web and modified to work with Wordpress and OpsWorks.

## Apache2

Configure Apache2 with FastCGI (php-fpm via socket) and the Event MPM

## AWS-Ubuntu

Configure an AWS Opsworks Ubuntu image with a swapspace. This is aimed at t1.micro instances to prevent "out of memory" issues.

Available recipes for [AWS OpsWorks Lifecycle Events](http://docs.aws.amazon.com/opsworks/latest/userguide/workingcookbook-events.html):
* **Setup**: aws-ubuntu::setup; Adds memory swap

## PHP-FPM

Configure php-fpm with custom php.ini

## S3FS

Mount an S3 bucket using custom JSON in OpsWorks:

```
"s3fs" : {
  "data_bag": {
        "id": "s3fs_deploy_key",
        "buckets": [ "my-bucket" ],
        "access_key_id": "my-access-key",
        "secret_access_key": "my-secret-key"
      }
}
```

## Varnish

Configure Varnish 4.0 to work with Wordpress and the [Varnish HTTP Purge plugin](https://wordpress.org/plugins/varnish-http-purge/)

## Wordpress

Configure Wordpress to interact with the MySQL server. It can be used for a fresh install or a restore from a Backup using [BackWPup](http://wordpress.org/plugins/backwpup/).

Available recipes for [AWS OpsWorks Lifecycle Events](http://docs.aws.amazon.com/opsworks/latest/userguide/workingcookbook-events.html):
* **Configure**: wordpress::configure; Create wp-config.php file along with Cronjob