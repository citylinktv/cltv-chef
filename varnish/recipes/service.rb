service 'varnish' do
  supports restart: true, reload: true
  action %w(enable start)
end

service 'varnishlog' do
  supports restart: true, reload: true
  action %w(enable start)
end