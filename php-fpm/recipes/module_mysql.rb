# from https://github.com/opscode-cookbooks/php/blob/master/recipes/module_mysql.rb

def el5_range
  (0..99).to_a.map { |i| "5.#{i}" }
end

pkg = value_for_platform(
  %w(centos redhat scientific fedora amazon oracle) => {
    el5_range => 'php53-mysql',
    'default' => 'php-mysql'
  },
  'default' => 'php5-mysql'
)

package pkg do
  action :install
end