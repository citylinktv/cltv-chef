#
# Author::  Seth Chisamore (<schisamo@opscode.com>)
# Cookbook Name:: php-fpm
# Recipe:: package
#
# Copyright 2011, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

template node['php-fpm']['conf_file'] do
  source "php-fpm.conf.erb"
  mode 00644
  owner node["php-fpm"]["user"]
  group node["php-fpm"]["group"]
  notifies :restart, "service[php-fpm]"
end

if node['php-fpm']['pools']
  node['php-fpm']['pools'].each do |k,v|
         self.params[k.to_sym] = v
  end
end

# from https://github.com/till/easybib-cookbooks/blob/master/php-fpm/recipes/configure.rb
template "/etc/php5/fpm/php.ini" do
  mode     "0755"
  source   "php.ini.erb"
  variables(
    :enable_dl => 'Off',
    :memory_limit => node["php-fpm"]["memorylimit"],
    :display_errors => node["php-fpm"]["displayerrors"],
    :max_execution_time => node["php-fpm"]["maxexecutiontime"],
    :max_input_vars => node["php-fpm"]["ini"]["max-input-vars"],
    :logfile => node["php-fpm"]["logfile"],
    :error_log => node["php-fpm"]["error_log"],
    :tmpdir => node["php-fpm"]["tmpdir"],
    :prefix => node["php-fpm"]["prefix"],
    :gc_maxlifetime => node["php-fpm"]["gc_maxlifetime"],
    :upload_max_filesize => node["php-fpm"]["upload_max_filesize"],
    :post_max_size => node["php-fpm"]["post_max_size"]
  )
  owner    node["php-fpm"]["user"]
  group    node["php-fpm"]["group"]
  notifies :reload, "service[php-fpm]", :delayed
end
