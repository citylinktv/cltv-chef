#
# Cookbook Name:: wowza_haproxy
# Recipe:: packages
#
# Copyright 2015, City Link TV
#
# All rights reserved - Do Not Redistribute
#

apt_repository "haproxy-1.5" do
  uri "http://ppa.launchpad.net/vbernat/haproxy-1.5/ubuntu"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "1C61B9CD"
end

execute "apt-get-update" do
  command "apt-get update"
  ignore_failure true
  action :nothing
end

package 'haproxy' do
    package_name 'haproxy'
    action :install
end

template 'haproxy.cfg' do
    path "/etc/haproxy/haproxy.cfg"
    owner 'root'
    group 'root'
end

template 'haproxy_default' do
    path "/etc/default/haproxy"
    owner 'root'
    group 'root'
end

service "haproxy" do
  action :enable
  action :restart
end