#
# Cookbook Name:: wowza
# Recipe:: packages
#
# Copyright 2015, City Link TV
#
# All rights reserved - Do Not Redistribute
#

wowza_tar = 'WowzaStreamingEngine-4.2.0.01.tar.gz'
wowza_sha = '3c19cec03aaad2d5c7425dd4cfa9c3a9f5abf8d6ae07d9980c7e13389429c464'
wowza_version_dir = '/usr/local/WowzaStreamingEngine-4.2.0'

wowza_base_dir   = '/usr/local/WowzaStreamingEngine'

if !File.exist? "/tmp/#{wowza_tar}" or !File.exist? wowza_version_dir
  remote_file "/tmp/#{wowza_tar}" do
    source "http://cltv-wowza.s3.amazonaws.com/#{wowza_tar}"
    checksum wowza_sha
    owner 'root'
    group 'root'
    mode '0644'
  end

  bash 'install wowza tarball' do
    user 'root'
    cwd '/usr/local'
    code <<-EOH
      tar xf /tmp/#{wowza_tar}
    EOH
  end

  link wowza_base_dir do
    to wowza_version_dir
  end
end

link '/usr/bin/WowzaStreamingEngined' do
  to "#{wowza_base_dir}/bin/WowzaStreamingEngined"
end

link '/usr/bin/WowzaStreamingEngineManagerd' do
  to "#{wowza_base_dir}/manager/bin/WowzaStreamingEngineManagerd"
end

link '/etc/init.d/WowzaStreamingEngine' do
  to "#{wowza_base_dir}/bin/WowzaStreamingEngine"
end

link '/etc/init.d/WowzaStreamingEngineManager' do
  to "#{wowza_base_dir}/manager/bin/WowzaStreamingEngineManager"
end