#
# Cookbook Name:: wowza
# Recipe:: newrelic
#
# Copyright 2015, City Link TV
#
# All rights reserved - Do Not Redistribute
#
# Configure Wowza Applications

wowza_base_dir   = '/usr/local/WowzaStreamingEngine'
wowza_nr_dir    = "#{wowza_base_dir}/newrelic"

# create newrelic dir
directory wowza_nr_dir do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

cookbook_file "#{wowza_nr_dir}/newrelic.jar"

template "#{wowza_nr_dir}/newrelic.yml" do
    source "newrelic.yml.erb"
    variables(
        :newrelic_key => node['newrelic']['license']
    )
end