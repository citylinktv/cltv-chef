#
# Cookbook Name:: wowza
# Recipe:: ebs
#
# Copyright 2015, City Link TV
#
# All rights reserved - Do Not Redistribute
#

wowza_device_id = "/dev/xvdc"
wowza_bag = node['wowza']['data_bag']
wowza_data_mountpoint = wowza_bag['wowza_data_mountpoint']

aws_ebs_volume "wowza_ebs_volume" do
  aws_access_key wowza_bag['awsAccessKeyId']
  aws_secret_access_key wowza_bag['awsSecretAccessKey']
  size 100
  device wowza_device_id
  action [ :create, :attach ]
end

# create a filesystem
execute 'mkfs' do
  command "mkfs -t ext4 #{wowza_device_id}"
  # only if it's not mounted already
  not_if "grep -qs #{wowza_data_mountpoint} /proc/mounts"
end

# we then create the target data folder
directory wowza_data_mountpoint do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

# now we can enable and mount it and we're done!
mount "#{wowza_data_mountpoint}" do
  device wowza_device_id
  fstype 'ext4'
  options 'noatime,nobootwait'
  action [:enable, :mount]
end