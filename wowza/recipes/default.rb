#
# Cookbook Name:: wowza
# Recipe:: default
#
# Copyright 2015, City Link TV
#
# All rights reserved - Do Not Redistribute
#


###
# Install wowza
###

include_recipe 'wowza::install'
include_recipe 'wowza::ebs'