#
# Cookbook Name:: wowza
# Recipe:: configure
#
# Copyright 2015, City Link TV
#
# All rights reserved - Do Not Redistribute
#
# Configure Wowza Applications

wowza_bag = node['wowza']['data_bag']
wowza_base_dir   = '/usr/local/WowzaStreamingEngine'
wowza_app_dir    = "#{wowza_base_dir}/applications"
wowza_conf_dir   = "#{wowza_base_dir}/conf"
wowza_lib_dir    = "#{wowza_base_dir}/lib"

# ensure no applications are pre-configured so we delete them all
directory wowza_app_dir do
  recursive true
  action :delete
end

# we then re-create the target applications folder
directory wowza_app_dir do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

wowza_app_list = %W{
  livepkgr
  livertsp
  vods3
}

wowza_template_list = %W{
  admin.password
  log4j.properties
  MediaCache.xml
  Server.xml
  VHost.xml
  Tune.xml
  livepkgr/Application.xml
  livertsp/Application.xml
  vods3/Application.xml
}

wowza_app_list.each do |app|
  directory "#{wowza_app_dir}/#{app}" do
    owner 'root'
    group 'root'
    mode  '0755'
  end
  directory "#{wowza_conf_dir}/#{app}" do
    owner 'root'
    group 'root'
    mode  '0755'
  end
end

wowza_template_list.each do |item|
  template "#{wowza_conf_dir}/#{item}" do
    source "#{item}.erb"
    variables(
      :awsAccessKeyId => wowza_bag['awsAccessKeyId'],
      :awsSecretAccessKey => wowza_bag['awsSecretAccessKey'],
      :s3Bucket => wowza_bag['s3Bucket'],
      :httpGateway => wowza_bag['httpGateway'],
      :wowza_data_mountpoint => wowza_bag['wowza_data_mountpoint']
    )
  end
end

file "#{wowza_conf_dir}/Server.license" do
  owner 'root'
  group 'root'
  mode  '0600'
  content wowza_bag['license']
end

wowza_jar_list = %W{
  city_link_tv.jar
  aws-java-sdk-1.3.22.jar
  commons-codec-1.3.jar
  httpclient-4.4.jar
  httpcore-4.4.jar
  jackson-core-asl-1.8.7.jar
  jackson-mapper-asl-1.8.7.jar
  joda-time-2.3.jar
}

wowza_jar_list.each do |item|
    cookbook_file "#{wowza_lib_dir}/#{item}"
end

# Wowza 4 services
%W{
  WowzaStreamingEngine
  WowzaStreamingEngineManager
}.each do |item|
  service item do
    supports :status => true, :restart => true
    action [ :enable, :restart ]
  end
end