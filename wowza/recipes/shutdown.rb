#
# Cookbook Name:: wowza
# Recipe:: shutdown
#
# Copyright 2015, City Link TV
#
# All rights reserved - Do Not Redistribute
#
# Shutdown Wowza Services

%W{
  WowzaStreamingEngine
  WowzaStreamingEngineManager
}.each do |item|
  service item do
    action :stop
  end
end